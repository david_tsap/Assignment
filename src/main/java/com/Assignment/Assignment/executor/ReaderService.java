package com.Assignment.Assignment.executor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.TimerTask;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.Assignment.Assignment.Log.logService;

@Service
@Scope("prototype")
public class ReaderService extends TimerTask{

	InputStream inputStream;
	
	@Autowired
	logService log;
	
	int LogId;

	public int init() {
		// can create new row in table and will return ID for easy search 
		// for now we will use one log 
		return 1;
	}

	@Override
	public void run() {
		try {
			byte[] targetArray = new byte[inputStream.available()];
			if(targetArray.length > 0) {
				inputStream.read(targetArray);
				String read = new String(targetArray);
				if(read != null)
					log.addNewLog(read);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void addInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

}
