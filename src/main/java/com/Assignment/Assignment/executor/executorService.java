package com.Assignment.Assignment.executor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.Assignment.Assignment.Log.logService;

@Service
public class executorService {
	
	@Autowired
	logService log;
	
	@Autowired
	ReaderService reader;
	
	@Autowired
	executeProcess executor;
	
	public int executCommand(String command, int frequency) {
		int id = reader.init();
		executor.execute(command, frequency, reader);
		return id;
	}

	public String getCurrentLog(int id) {
		return log.getCurrentLog(id);
		
	}
}
