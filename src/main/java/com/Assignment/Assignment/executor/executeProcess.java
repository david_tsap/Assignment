package com.Assignment.Assignment.executor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;


@Service
@Scope("prototype")
public class executeProcess {

	public void execute(String command, int frequency,ReaderService schedule) {
		Executor executor = Executors.newSingleThreadExecutor(); 
		executor.execute(new Runnable() {
			@Override
			public void run() {
				Process process;
				try {
					process = Runtime.getRuntime().exec("cmd /c " + command);
					InputStream inputStream = process.getInputStream();
					schedule.addInputStream(inputStream);
					new Timer().scheduleAtFixedRate( 
							schedule, 0,
							frequency * 100 
							);
					process.waitFor();
					schedule.cancel();
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		});

	}
}
