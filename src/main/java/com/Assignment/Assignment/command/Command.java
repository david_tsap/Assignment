package com.Assignment.Assignment.command;

public class Command { // will change to DB if needed
	
	private int Logid; // log ID to find command logs
	
	private String command;

	private int frequency;
	

	public int getId() {
		return Logid;
	}

	public void setId(int Logid) {
		this.Logid = Logid;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

}
