package com.Assignment.Assignment.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.Assignment.Assignment.executor.executorService;
import com.Assignment.Assignment.Log.logService;

@Service
public class CommandService {

	@Autowired
	executorService executorService; // can change to API for another Microservices
	
	@Autowired
	logService logService; // can change to API for another Microservices
	
	
	public Command executCommand(String command, int frequency) {

		int id = executorService.executCommand(command, frequency);
		//will add command to DB and will return The command object
		// for now we will create manually.
		Command cmd = new Command();
		cmd.setCommand(command);
		cmd.setFrequency(frequency);
		cmd.setId(id);
		return cmd;
	}

	public String getCurrentLog(int logId) {
		return logService.getCurrentLog(logId);
	}

}
