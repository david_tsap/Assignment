package com.Assignment.Assignment.command;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Assignment.Assignment.executor.executorService;


@RestController
@RequestMapping("api/commandExecutor")
public class CommandController {
	
	@Autowired
	CommandService executor;
	
	
    @PostMapping
    public Command executCommand(@RequestBody Command command) {
    	return executor.executCommand(command.getCommand(), command.getFrequency());
    }
    
    @GetMapping(path = "{logId}")
    public String getLog(@PathVariable("logId") int logId) {
    	return executor.getCurrentLog(logId);
    }

}
