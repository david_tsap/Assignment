package com.Assignment.Assignment.Log;


import java.util.List;


public class Log{ // will change imp to DB impl
	
	private List<String> fullLog;
	private String currentLog;
	private String lastUpdated;
	
	public List<String> getFullLog() {
		return fullLog;
	}
	public void setFullLog(List<String> fullLog) {
		this.fullLog = fullLog;
	}
	public String getCurrentLog() {
		return currentLog;
	}
	public void setCurrentLog(String currentLog) {
		this.currentLog = currentLog;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Override
	public String toString() {
		return lastUpdated + " - " + currentLog;
	}
	

	
	
	
	

}
