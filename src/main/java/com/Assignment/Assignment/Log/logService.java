package com.Assignment.Assignment.Log;

import org.springframework.stereotype.Service;

@Service
public class logService {
	
	Logger testLog = new TestLog();
	
	public String getCurrentLog(int id) {
		return testLog.getCurrentLog(id);
	}

	public void addNewLog(String log) {
		this.testLog.addNewLog(log);
		
	}

}
