package com.Assignment.Assignment.Log;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class TestLog implements Logger{
	
	//currently we work only with local log , will change and fetch by ID
	Log log = new Log(); // will replace with logger DB
	

	public void addNewLog(String logs){
		if(log.getFullLog() == null) {
			log.setFullLog(new ArrayList<String>());
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  // use to see changes in output
		log.getFullLog().add(logs);
		log.setLastUpdated(dtf.format(now));
		log.setCurrentLog(logs);
		//System.out.println(logs);
	}
	
	public String getCurrentLog(int id) {
		return log.toString(); // use the time to see changes
								// can replace only to current log or object
	}

}
